/*****************************************************************
* Module Name: Mongoose Models
* Description: Mongoose Models of webForm server
* Author:
* Date Modified: 2016.09.27
* Related files: n/a
*****************************************************************/
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//Models for mongoose to communicate to DB and as an object to play in Code.

exports.Sensor = new Schema({
  rawData   : {type: String, trim: true},
  sensorID  : {type: String, required: true, trim: true},
  sensorType     : {type: String, trim: true},
  reading1   : {type: Number, trim: true},
  reading2        : {type: Number, trim: true},
  reading3       : {type: Number, trim: true},
  createdDate     : {type: Date, trim: true}
});


exports.User = new Schema({
  loginID      : {type: String, trim: true},
  password     : {type: String, trim: true},
  email        : {type: String, trim: true},
  userType     : {type: String, trim: true},
  lang        : {type: String, trim: true},
  createdBy    : {type: String, trim: true},
  createdDate  : {type: Date,   trim: true},
  updatedBy    : {type: String, trim: true},
  updatedDate  : {type: Date,   trim: true}
});
