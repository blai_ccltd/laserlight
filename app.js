/*****************************************************************
* Module Name: Main router
* Description: Main router of webform server
* Author: Brian Lai
* Date Modified: 2016.07.25
* Related files: n/a
*****************************************************************/
// Module dependencies.
var express = require('express');
var mongoose = require('mongoose');
var models = require('./models/models');
//Add all routes
var routes = require('./routes');
var login = require('./routes/login');
//var user = require('./routes/user');
var sensor = require('./routes/sensor');

//var md5 = require('md5');
var http = require('http');
var https = require('https');
var fs = require('fs');
var path = require('path');
var cors = require('cors');
var util = require('util');
var favicon = require('serve-favicon');
var logger = require('morgan');
var multer = require('multer');
var methodOverride = require('method-override');
var session = require('express-session');
var bodyParser = require('body-parser');
var errorHandler = require('errorhandler');
var app = express();

var log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
var log_stdout = process.stdout;

console.log = function(d) { //
  log_file.write(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + ' | ' +  util.format(d) + '\n');
  log_stdout.write(new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '') + ' | ' +  util.format(d) + '\n');
};

//all environments
app.set('httpPort', process.env.PORT || 5080);
app.set('httpsPort', process.env.PORT || 5333);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
//DB path, need to change to read property file
app.set('dbURL', 'localhost:27017');
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(methodOverride());
app.use(session({ resave: true,
  saveUninitialized: true,
  secret: 't3st12'}));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());

var options = {
 //key  : fs.readFileSync('server.enc.key'),
 //cert : fs.readFileSync('server.crt'),
 //passphrase: 'ccAcc0unt'
};

//Create DB connection and test if it connected.
var connection = mongoose.createConnection('mongodb://' + app.get('dbURL') + "/sensor");
connection.on('error', console.error.bind(console, 'DB connection error: '));
connection.once('open', function() {
	console.log('Connected to DB');
});



// Add db models into req object
function db(req, res, next) {
  req.db = {
    User           : connection.model('User', models.User, 'users'),
    Sensor  : connection.model('Sensor', models.Sensor, 'sensors')
    //TradePartner  : connection.model('TradePartner', models.TradePartner, 'tradePartner')
  };

  return next();
}



// check client token
app.use(function(req,res,next){
  return next();
});//End of app.use function



// development only
if ('development' === app.get('env')) {
  app.use(errorHandler());
}

//Routes. The first parameter: path to match. parameter after: functions to run in sequence.
app.post('/', cors(), login.auth);
app.post('/rawdata', cors(), db, sensor.postRawData);
app.post('/sensorLatest', cors(), db, sensor.getSensorLatest);

//start the server.
var httpsServer = https.createServer(options, app);
httpsServer.listen(
  app.get('httpsPort'),
  function(){
  	console.log('Express server listening on '	+ httpsServer.address().address + 'port ' + httpsServer.address().port);
});

var httpServer = http.createServer(app);
  httpServer.listen(
  app.get('httpPort'),
  function() {
    console.log('Express server listening on '	+ httpServer.address().address + 'port ' + httpServer.address().port);
});
