/*****************************************************************
* Module Name: Login router
* Description: Login router of Ahitis server
* Author: Brian Lai
* Date Modified: 2016.01.04
* Related files: n/a
*****************************************************************/
//Require the md5 module.
var md5 = require('md5');
//var nodemailer = require('nodemailer');

//The purpose of this login router is to obtain the user from DB and check if the password match.
//If match, generate an accesstoken for the user. and send back to the user.
exports.auth = function(req, res) {
	return res.status(200).send("1");
}
