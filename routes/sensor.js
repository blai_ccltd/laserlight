/*****************************************************************
* Module Name: Orders
* Description: WebForm orders business/data logic implementation.
* Author: dredmonds
* Date Modified: 2016.09.29
* Related files: n/a
*****************************************************************/

var objectID = require('bson-objectid'); //Required modules for objectId generation.
var async = require("async");

var hex2decimal = function(s){
  var digits = "0123456789ABCDEF";
  s = s.toUpperCase();
  var val = 0;
  for(var i = 0; i<s.length;i++){
    var c = s.substring(i,i+1);
    var d = digits.indexOf(c);
    val = 16*val+d;
  }
  return val;
};

var hex2Binary = function(s){
  let bytes = [];
  let str = "";

  for(let i=0; i< s.length-1; i+=2){
    bytes.push(parseInt(s.substr(i, 2), 16));
  }

  str = String.fromCharCode.apply(String, bytes);

  return str;
};

exports.getSensorLatest = function(req,res){
  var result = {};
  var sensorID = req.body.sensorID;

  console.log("getSensorLatest entered");
  req.db.Sensor.find({sensorID: sensorID},{},{sort: {createdDate:-1}, limit: 1})
  .exec(function(err, sensor){
    console.log("Sensor find executed");
    if(err){
      console.log("Error find sensor latest: " + sensorID);
      console.log(err.message);
      result.result = "Error find sensor latest: " + sensorID;
      res.status(500).json(result);
      return;
    }
    else{
      console.log("Sensor find no error");
      if(sensor && sensor.length > 0){
        console.log("Sensor find");
        console.log(JSON.stringify(sensor));
        result.sensorID = sensor[0].sensorID;
        result.sensorType = sensor[0].sensorType;
        result.reading1 = sensor[0].reading1;
        result.reading2 = sensor[0].reading2;
        result.reading3 = sensor[0].reading3;
        result.createdDate = sensor[0].createdDate;

        console.log(JSON.stringify(result));
        res.status(200).json(result);
        return;
      }else{
        console.log("No sensor found: " + sensorID);
        result.result = "No sensor found: " + sensorID;
        res.status(500).json(result);
        return;
      }
    }
  })
}

exports.postRawData = function(req, res){
  var result = {};
  var rawData = req.body.data;
  var sensorDBObjs = [];

  var lines = rawData.split("-");

  lines.forEach(function(line){
    let sensorID = "";
    let sensorType = "";
    let reading1 = 0;
    let reading2 = 0;
    let reading3 = 0;

    sensorType = line.substring(2,4);

    if(sensorType.substring(0,1) == "A"){
      //partical sensor
      sensorID = line.substring(2,6);

      let hexReading1High = line.substring(6,8);
      let hexReading1Low = line.substring(8,10);
      let hexReading2 = line.substring(10,12);
      let hexReading3High = line.substring(12,14);
      let hexReading3Low = line.substring(14,16);
      let createdDate = Date.parse(line.substring(20,24) + "-" + line.substring(24,26) + "-" + line.substring(26,28) + "T" + line.substring(28,30) + ":" + line.substring(30,32) + ":" + line.substring(32,34) + ".000Z");

      reading1 = ((hex2decimal(hexReading1High)*256) + hex2decimal(hexReading1Low)) / 10;
      reading2 = hex2decimal(hexReading2);
      reading3 = ((hex2decimal(hexReading3High)*256) + hex2decimal(hexReading3Low));

      let sensorModel = {
        rawData: line,
        sensorID: sensorID,
        sensorType: sensorType,
        reading1: reading1,
        reading2: reading2,
        reading3: reading3,
        createdDate: createdDate,
      };

      let sensorDBObj = new req.db.Sensor(sensorModel);
      sensorDBObjs.push(sensorDBObj);
    }else if(sensorType == "02"){
      //Internal temp. sensorID
      sensorID = line.substring(2,10);

      let statusByte = line.substring(14,16);
      let statusBitString = hex2Binary(statusByte);
      let negative1 = statusBitString.substring(6,7);
      let negative2 = statusBitString.substring(5,6);
      let reading1 = hex2decimal(line.substring(10,12));

      let createdDate = Date.parse(line.substring(20,24) + "-" + line.substring(24,26) + "-" + line.substring(26,28) + "T" + line.substring(28,30) + ":" + line.substring(30,32) + ":" + line.substring(32,34) + ".000Z");

      reading1 = reading1 / 5;

      if(negative1 == "1"){
        reading1 = 0 - reading1;
      }

      let sensorModel = {
        rawData: line,
        sensorID: sensorID,
        sensorType: sensorType,
        reading1: reading1,
        reading2: reading2,
        reading3: reading3,
        createdDate: createdDate,
      };

      let sensorDBObj = new req.db.Sensor(sensorModel);
      sensorDBObjs.push(sensorDBObj);
    }
  });

  async.each(sensorDBObjs, function(saveDBObj,next){
    saveDBObj.save(function(err){
      return next(err);
    });
  },function(err){
    if(err){
      console.log("RawData: " + rawData);
      result.result = "Error inserted new sensor record: " + rawData;
      res.status(500).json(result);
      return;
    }else{
      console.log("RawData: " + rawData);
      result.result = "Inserted new sensor record: " + rawData;
      res.status(200).json(result);
      return;
    }
  });

};//End of postRawData function
